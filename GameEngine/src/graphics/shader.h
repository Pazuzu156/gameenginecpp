#pragma once

#ifndef SHADER_H
#define SHADER_H

#include <GL/glew.h>
#include <vector>
#include <iostream>

#include "../math/math.h"
#include "../utils/fileutils.h"

namespace kaleb
{
	namespace graphics
	{
		class Shader
		{
		public:
			Shader(const char* vertPath, const char* fragPath);
			~Shader();

			void setUniform1f(const GLchar* name, float value);
			void setUniform1i(const GLchar* name, int value);
			void setUniform2f(const GLchar* name, const math::vec2& vector);
			void setUniform3f(const GLchar* name, const math::vec3& vector);
			void setUniform4f(const GLchar* name, const math::vec4& vector);
			void setUniformMat4(const GLchar* name, const math::mat4& matrix);

			void enable() const;
			void disable() const;
		private:
			GLint getUniformLocation(const GLchar* name);
			GLuint m_ShaderID;
			const char* m_VertPath;
			const char* m_FragPath;

			GLuint load();
		};
	}
}

#endif // SHADER_H